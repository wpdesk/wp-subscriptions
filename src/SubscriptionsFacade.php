<?php

namespace WPDesk\Subscriptions;


use WPDesk\PluginBuilder\Plugin\Hookable;
use WPDesk\Subscriptions\License\LicensePage;

class SubscriptionsFacade implements Hookable {
	const LIBRARY_TEXT_DOMAIN = 'wpdesk-helper';

	/**
	 * Prevents attaching hooks more than once when used in many plugins
	 *
	 * @var bool
	 */
	private static $already_hooked;

	public function hooks() {
		if ( is_admin() && ! self::$already_hooked ) {
			self::$already_hooked = true;

			$plugin_registry = new InstalledPlugins();
			$license_page    = new LicensePage( $plugin_registry );

			$integration = new HelperIntegration( $license_page, $plugin_registry );
			$integration->integrate();
			$this->init_translations();
		}
	}

	/**
	 * Adds text domain used in a library
	 */
	private function init_translations() {
		if ( function_exists( 'determine_locale' ) ) {
			$locale = determine_locale();
		} else { // before WP 5.0 compatibility
			$locale = get_locale();
		}
		$locale = apply_filters( 'plugin_locale', $locale, self::LIBRARY_TEXT_DOMAIN );

		$mofile = __DIR__ . '/../lang/' . self::LIBRARY_TEXT_DOMAIN . '-' . $locale . '.mo';
		if ( file_exists( $mofile ) ) {
			load_textdomain( self::LIBRARY_TEXT_DOMAIN, $mofile );
		}
	}
}