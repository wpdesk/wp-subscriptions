<?php

namespace WPDesk\Subscriptions\License;

/**
 * Action that can be executed relative to plugin.
 *
 * @package WPDesk\Subscriptions\License
 */
interface Action {
	public function execute( array $plugin );
}