<?php

namespace WPDesk\Subscriptions\License\Action;

use WPDesk\Subscriptions\License\Action;

/**
 * Do nothing.
 *
 * @package WPDesk\Subscriptions\License\Action
 */
class Nothing implements Action {
	public function execute( array $plugin ) {
		// NOOP
	}
}