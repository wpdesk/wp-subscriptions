<?php

namespace WPDesk\Subscriptions\License;

use WPDesk\Subscriptions\License\Action\LicenseActivation;
use WPDesk\Subscriptions\License\Action\LicenseDeactivation;
use WPDesk\Subscriptions\License\Action\Nothing;

/**
 * Action factory.
 *
 * @package WPDesk\Subscriptions\License
 */
class LicensePageActions {
	/**
	 * Creates action object according to given param
	 *
	 * @param string $action
	 *
	 * @return Action
	 */
	public function create_action( $action ) {
		if ( $action === 'activate' ) {
			return new LicenseActivation();
		}

		if ( $action === 'deactivate' ) {
			return new LicenseDeactivation();
		}

		return new Nothing();
	}
}
