<?php

namespace WPDesk\Subscriptions;


use WPDesk\PluginBuilder\Storage\Exception\ClassNotExists;
use WPDesk\PluginBuilder\Storage\StorageFactory;
use WPDesk\Subscriptions\License\LicensePage;

/**
 * Replaces activate/update/license page in WPDesk_Helper
 *
 * @package WPDesk\Subscriptions
 */
class HelperIntegration {
	/** @var LicensePage */
	private $license_page;

	/** @var InstalledPlugins */
	private $plugin_registry;

	const PRIORITY_HELPER_UPDATE = 9999999;
	const MAIN_WPDESK_MENU_POSITION = 99.99941337;

	const CALLBACK_KEY = 'function';
	const CALLBACK_KEY_WITH_FUNCTION_NAME = 1;

	public function __construct( LicensePage $license_page, InstalledPlugins $plugin_registry ) {
		$this->license_page    = $license_page;
		$this->plugin_registry = $plugin_registry;
	}

	/**
	 * @return void
	 */
	public function integrate() {
		$this->license_page->hooks();

		$this->replace_helper_updates();
		$this->replace_helper_license_page();
		$this->hide_notice_about_helper_need();

	}

	/**
	 * @return void
	 */
	/**
	 * @return void
	 */
	private function replace_helper_updates() {
		// have to be in plugins_loaded because helper is loaded simultaneously with this plugin
		add_action( 'plugins_loaded',
			function () {
				remove_action( 'plugins_loaded',
					[ $this->get_helper_instance(), 'init_helper_plugins' ],
					self::PRIORITY_HELPER_UPDATE );
				add_action( 'plugins_loaded',
					[ $this->plugin_registry, 'refresh_plugin_update_info' ],
					self::PRIORITY_HELPER_UPDATE );
			} );
	}

	/**
	 * @return void
	 */
	private function replace_helper_license_page() {
		add_action( 'admin_menu', [ $this, 'handle_replace_helper_license_page' ] );
	}

	/**
	 * @return void
	 */
	private function hide_notice_about_helper_need() {
		remove_action( 'admin_notices', 'wpdesk_helper_notice' );

		$this->remove_object_action_by_name( 'admin_notices', 10, 'wpdesk_helper_notice' );
	}

	/**
	 * Removes action/filter using object method callbacks name. You don't need an object instance to use this.
	 *
	 * @param string $action_name
	 * @param int    $priority
	 * @param string $function_name
	 */
	private function remove_object_action_by_name( $action_name, $priority, $function_name ) {
		global $wp_filter;
		/** @var \WP_Hook $admin_notices_tag */
		$admin_notices_tag = $wp_filter[ $action_name ];
		if ( isset( $admin_notices_tag->callbacks[ $priority ] ) ) {
			$default_priority_callbacks = $admin_notices_tag->callbacks[ $priority ];

			foreach ( $default_priority_callbacks as $callback ) {
				if ( is_array( $callback ) && is_array( $callback[ self::CALLBACK_KEY ] ) && isset( $callback[ self::CALLBACK_KEY ][ self::CALLBACK_KEY_WITH_FUNCTION_NAME ] ) ) {
					$found_function_name = $callback[ self::CALLBACK_KEY ][ self::CALLBACK_KEY_WITH_FUNCTION_NAME ];
					if ( $found_function_name === $function_name ) {
						$admin_notices_tag->remove_filter( $action_name, $callback[ self::CALLBACK_KEY ], $priority );
					}
				}
			}
		}
	}

	/**
	 * Replace licenses page if helper exists or add that page if helper not exists.
	 * Have to be called from admin_menu action.
	 *
	 * @return void
	 */
	public function handle_replace_helper_license_page() {
		$wpdesk_helper = $this->get_helper_instance();

		if ( $wpdesk_helper ) {
			// remove submenu and hook for submenu
			remove_submenu_page( 'wpdesk-helper', LicensePage::PAGE_SLUG );
			remove_action( 'wp-desk_page_wpdesk-licenses', [ $wpdesk_helper, 'wpdesk_licenses' ] );

			// remove settings submenu as we want to reorder whole menu
			remove_submenu_page( 'wpdesk-helper', 'wpdesk-helper-settings' );
		} else {
			add_menu_page( 'WP Desk',
				'WP Desk',
				'manage_options',
				'wpdesk-helper',
				[
					$this,
					'render_wpdesk_licenses_page',
				],
				'dashicons-controls-play',
				self::MAIN_WPDESK_MENU_POSITION );
		}

		$this->license_page->handle_add_page_submenu_item();

		if ( $wpdesk_helper ) {
			// readd settings submenu as we want to reorder whole menu
			add_submenu_page( 'wpdesk-helper',
				__( 'Settings', 'wpdesk-helper' ),
				__( 'Settings', 'wpdesk-helper' ),
				'manage_options',
				'wpdesk-helper-settings',
				[ $wpdesk_helper, 'wpdesk_helper_settings' ]
			);
		} else {
			// remove default submenu as we dont need it
			remove_submenu_page( 'wpdesk-helper', 'wpdesk-helper' );
		}
	}

	/**
	 * Fetches WPDesk_Helper instance if it's available.
	 *
	 * @return \WPDesk_Helper|null
	 */
	private function get_helper_instance() {
		$storage = new StorageFactory();
		try {
			return $storage->create_storage()->get_from_storage( 'WPDesk_Helper' );
		} catch ( ClassNotExists $e ) {
			return null;
		}
	}
}
