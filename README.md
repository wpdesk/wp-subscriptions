[![pipeline status](https://gitlab.com/wpdesk/wp-subscriptions/badges/master/pipeline.svg)](https://gitlab.com/wpdesk/wp-subscriptions/commits/master) 
Integration: [![coverage report](https://gitlab.com/wpdesk/wp-subscriptions/badges/master/coverage.svg?job=integration+test+lastest+coverage)](https://gitlab.com/wpdesk/wp-subscriptions/commits/master)
Unit: [![coverage report](https://gitlab.com/wpdesk/wp-subscriptions/badges/master/coverage.svg?job=unit+test+lastest+coverage)](https://gitlab.com/wpdesk/wp-subscriptions/commits/master)

wp-subscriptions
====================
