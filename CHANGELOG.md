## [1.1.5] - 2019-04-10
### Fixed
- Fixed fatal error when some clousures are hooked in admin_notices #2

## [1.1.4] - 2019-04-04
### Fixed
- Fixed fatal error when some clousures are hooked in admin_notices

## [1.1.3] - 2019-03-28
### Fixed
- WordPress <5.0 compatibility PB-380

## [1.1.2] - 2019-03-27
### Fixed
- Fixed forced locale in translations PB-349

## [1.1.1] - 2019-03-26
### Fixed
- Fixed helper action removal PB-351

## [1.1.0] - 2019-03-26
### Added
- Supports translations

## [1.0.1] - 2019-03-25
### Fixed
- Can remove info about helper requirement in more ways
- Checks if WPDesk_Logger_Factory exists before using

## [1.0.0] - 2019-03-25
### Added
- Subscriptions